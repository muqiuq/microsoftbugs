# Outlook + Exchange Server 2019 (on premise) - Shared Inbox folders not updating on new e-mail

## Solution
**Cause:** If an email address is simultaneously defined in an on-premise Exchange Server and used as a Microsoft Teams group email, it leads to this issue.

**Solution:** A subdomain was added to Microsoft 365, and all group emails were changed to this new subdomain.

Until now, I have been unable to locate the documentation for this. Please inform me if you manage to find it.

## Background and description

I’ve been chasing an issue with our Exchange Server 2019 for a while now. 

Overview: 

 - One Exchange Server 2019 CU12 On Premise (Installed on a Windows Server 2019)
 - One NGINX Reverse Proxy 
 - One Postfix, rspamd, clamav E-Mail Filter (on a separate Debian VM)
 - Windows 10 Pro Clients with Outlook

With one of the Outlook clients for a specific user I have the following issue: Outlook does not refreshes inboxes of Shared Mailboxes (Example: New E-Mail arrives in the inbox: Outlook does not updates the inbox, user does not sees new e-mail until he updates the folder manually using "Update folder" in the ribbon menu. ). 

What I tried so far: 
 - Reinstall Windows and Office (Tried 3 times), 
 - Reset Outlook profile (Tried more then 50 times)
 - HealthChecker.ps1 => No issues found
 - Repaired Exchange Database
 - Delete Mailbox of User and recreate it
 - Delete Shared Mailboxes and recreate them
 - Delete one of the not synchronizing shared mailboxes and recreate it
 - https://support.microsoft.com/en-us/office/shared-mailbox-is-not-automatically-refreshing-for-new-email-in-outlook-94b97f45-c57c-45f3-9716-f3f6bec47c73

More observations: 
 - Everything works fine, when I change to "Online Mode", but the performance is terrible. ==> This is not an option
 - No issues when using Outlook in the Browser (OWA)


In my analsys I figured out the following:

Outlook makes a request to
```
/EWS/Exchange.asmx
```

Request:
```xml
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Header>
<RequestServerVersion Version="Exchange2015" xmlns="http://schemas.microsoft.com/exchange/services/2006/types"/>
<MailboxCulture xmlns="http://schemas.microsoft.com/exchange/services/2006/types">de-DE</MailboxCulture>
</s:Header><s:Body><ChannelSubscribe xmlns="http://schemas.microsoft.com/exchange/services/2006/messages">
<ChannelId>8F1FDBF3-78DC-46DF-BADA-97DAB4120A39</ChannelId>
<Subscriptions><ActivityFeedNotificationCountSubscription xmlns="http://schemas.microsoft.com/exchange/services/2006/types"><SubscriptionId>F73E77E6-7E74-2904-3B23-D5EAA2F97B3D</SubscriptionId>
</ActivityFeedNotificationCountSubscription></Subscriptions>
</ChannelSubscribe></s:Body></s:Envelope>
```

And gets an error (response):
```xml
<?xml version="1.0" encoding="utf-8"?><s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
<s:Body><s:Fault>
<faultcode xmlns:a="http://schemas.microsoft.com/exchange/services/2006/types">a:ErrorInternalServerError</faultcode>
<faultstring xml:lang="de-CH">Interner Serverfehler. Fehler bei diesem Vorgang.</faultstring>
<detail><e:ResponseCode xmlns:e="http://schemas.microsoft.com/exchange/services/2006/errors">ErrorInternalServerError</e:ResponseCode>
<e:Message xmlns:e="http://schemas.microsoft.com/exchange/services/2006/errors">Interner Serverfehler. Fehler bei diesem Vorgang.</e:Message>
</detail></s:Fault></s:Body></s:Envelope>
```

When I check the EWS log:
```
Microsoft Office/16.0 (Windows NT 10.0; Microsoft Outlook 16.0.15831; Pro)
Target=None;Req=Exchange2015/Exchange2016
ChannelSubscribe
,500,749,,ErrorInternalServerError
```


```
ServiceTaskMetadata.WatsonReportCount=1;S:WLM.Bal=298978;S:ServiceTaskMetadata.ServiceCommandBegin=25;S:ServiceTaskMetadata.ServiceCommandEnd=1377;S:ActivityStandardMetadata.Component=Ews;S:WLM.BT=Ews;S:EwsMetadata.HttpHandlerGetterLatency=0;Dbl:WLM.TS=1385;I32:ADS.C[anlikerdc01]=2;F:ADS.AL[anlikerdc01]=1.2761;Dbl:CCpu.T[CMD]=375;Dbl:BudgUse.T[]=1351.00756835938;I32:ADS.C[anlikerdc02]=1;F:ADS.AL[anlikerdc02]=2.0032;I32:ATE.C[anlikerdc02.ad.anliker-alarm.ch]=0;F:ATE.AL[anlikerdc02.ad.anliker-alarm.ch]=0,,BaseServiceTask_SendWatsonReportOnGrayException=Microsoft.Exchange.Common.GrayException 

System.ArgumentNullException: Der Wert darf nicht NULL sein. Parametername: results
    bei Microsoft.Exchange.Services.Core.Types.ServiceResult`1.ProcessServiceResults(IMinimalCallContext callContext  ServiceResult`1[] results  ProcessServiceResult`1 processDelegate)
    bei Microsoft.Exchange.Services.Core.ChannelSubscribe.GetResponse()
    bei Microsoft.Exchange.Services.Core.ServiceCommandBase`1.PostExecute()
    bei Microsoft.Exchange.Services.Core.Types.ServiceTask`1.<>c__DisplayClass11_0.<ExecuteHelper>b__0()
    bei Microsoft.Exchange.Common.IL.ILUtil.DoTryFilterCatch(Action tryDelegate  Func`2 filterDelegate  Action`1 catchDelegate)    --- Ende der internen Ausnahmestapelüberwachung ---
    bei Microsoft.Exchange.Common.GrayException.ExceptionCatcher(Object exception)
    bei Microsoft.Exchange.Common.IL.ILUtil.DoTryFilterCatch(Action tryDelegate  Func`2 filterDelegate  Action`1 catchDelegate)
    bei Microsoft.Exchange.Services.Core.Types.BaseServiceTask`1.SendWatsonReportOnGrayException(Action callback  Action exceptionHandlerCallback  Boolean isGrayExceptionTaskFailure);BaseServiceTask_FinishRequest=Microsoft.Exchange.Common.GrayException 

System.ArgumentNullException: Der Wert darf nicht NULL sein. Parametername: results
    bei Microsoft.Exchange.Services.Core.Types.ServiceResult`1.ProcessServiceResults(IMinimalCallContext callContext  ServiceResult`1[] results  ProcessServiceResult`1 processDelegate)
    bei Microsoft.Exchange.Services.Core.ChannelSubscribe.GetResponse()
    bei Microsoft.Exchange.Services.Core.ServiceCommandBase`1.PostExecute()
    bei Microsoft.Exchange.Services.Core.Types.ServiceTask`1.<>c__DisplayClass11_0.<ExecuteHelper>b__0()
    bei Microsoft.Exchange.Common.IL.ILUtil.DoTryFilterCatch(Action tryDelegate  Func`2 filterDelegate  Action`1 catchDelegate)    --- Ende der internen Ausnahmestapelüberwachung ---
    bei Microsoft.Exchange.Common.GrayException.ExceptionCatcher(Object exception)
    bei Microsoft.Exchange.Common.IL.ILUtil.DoTryFilterCatch(Action tryDelegate  Func`2 filterDelegate  Action`1 catchDelegate)
    bei Microsoft.Exchange.Services.Core.Types.BaseServiceTask`1.SendWatsonReportOnGrayException(Action callback  Action exceptionHandlerCallback  Boolean isGrayExceptionTaskFailure);BaseServiceTask_CompleteWCFRequest=Microsoft.Exchange.Common.GrayException 

System.ArgumentNullException: Der Wert darf nicht NULL sein. Parametername: results
    bei Microsoft.Exchange.Services.Core.Types.ServiceResult`1.ProcessServiceResults(IMinimalCallContext callContext  ServiceResult`1[] results  ProcessServiceResult`1 processDelegate)
    bei Microsoft.Exchange.Services.Core.ChannelSubscribe.GetResponse()
    bei Microsoft.Exchange.Services.Core.ServiceCommandBase`1.PostExecute()
    bei Microsoft.Exchange.Services.Core.Types.ServiceTask`1.<>c__DisplayClass11_0.<ExecuteHelper>b__0()
    bei Microsoft.Exchange.Common.IL.ILUtil.DoTryFilterCatch(Action tryDelegate  Func`2 filterDelegate  Action`1 catchDelegate)    --- Ende der internen Ausnahmestapelüberwachung ---
    bei Microsoft.Exchange.Common.GrayException.ExceptionCatcher(Object exception)
    bei Microsoft.Exchange.Common.IL.ILUtil.DoTryFilterCatch(Action tryDelegate  Func`2 filterDelegate  Action`1 catchDelegate)
    bei Microsoft.Exchange.Services.Core.Types.BaseServiceTask`1.SendWatsonReportOnGrayException(Action callback  Action exceptionHandlerCallback  Boolean isGrayExceptionTaskFailure);FaultInnerException=Microsoft.Exchange.Services.Core.Types.ErrorInternalServerErrorException: Interner Serverfehler. Fehler bei diesem Vorgang. 

System.ArgumentNullException: Der Wert darf nicht NULL sein. Parametername: results
    bei Microsoft.Exchange.Services.Core.Types.ServiceResult`1.ProcessServiceResults(IMinimalCallContext callContext  ServiceResult`1[] results  ProcessServiceResult`1 processDelegate)
    bei Microsoft.Exchange.Services.Core.ChannelSubscribe.GetResponse()
    bei Microsoft.Exchange.Services.Core.ServiceCommandBase`1.PostExecute()
    bei Microsoft.Exchange.Services.Core.Types.ServiceTask`1.<>c__DisplayClass11_0.<ExecuteHelper>b__0()
    bei Microsoft.Exchange.Common.IL.ILUtil.DoTryFilterCatch(Action tryDelegate  Func`2 filterDelegate  Action`1 catchDelegate)    --- Ende der internen Ausnahmestapelüberwachung ---
    bei Microsoft.Exchange.Services.Wcf.EWSService.GetServiceAsyncResult[TSoapResponseBody](IAsyncResult result)
    bei Microsoft.Exchange.Services.Wcf.EWSService.CreateSoapResponse[TSoapResponse TSoapResponseBody](IAsyncResult result  Func`2 createSoapResponseCallback)
    bei AsyncInvokeEndEndChannelSubscribe(Object   Object[]   IAsyncResult )
    bei System.ServiceModel.Dispatcher.AsyncMethodInvoker.InvokeEnd(Object instance  Object[]& outputs  IAsyncResult result)
    bei System.ServiceModel.Dispatcher.DispatchOperationRuntime.InvokeEnd(MessageRpc& rpc)
    bei System.ServiceModel.Dispatcher.ImmutableDispatchRuntime.ProcessMessage7(MessageRpc& rpc)
    bei System.ServiceModel.Dispatcher.MessageRpc.Process(Boolean isOperationContextSet);ExceptionHandlerBase_ProvideFault_Error=Microsoft.Exchange.Services.Core.Types.ErrorInternalServerErrorException: Interner Serverfehler. Fehler bei diesem Vorgang. 

System.ArgumentNullException: Der Wert darf nicht NULL sein. Parametername: results
    bei Microsoft.Exchange.Services.Core.Types.ServiceResult`1.ProcessServiceResults(IMinimalCallContext callContext  ServiceResult`1[] results  ProcessServiceResult`1 processDelegate)
    bei Microsoft.Exchange.Services.Core.ChannelSubscribe.GetResponse()
    bei Microsoft.Exchange.Services.Core.ServiceCommandBase`1.PostExecute()
    bei Microsoft.Exchange.Services.Core.Types.ServiceTask`1.<>c__DisplayClass11_0.<ExecuteHelper>b__0()
    bei Microsoft.Exchange.Common.IL.ILUtil.DoTryFilterCatch(Action tryDelegate  Func`2 filterDelegate  Action`1 catchDelegate)    --- Ende der internen Ausnahmestapelüberwachung ---
    bei Microsoft.Exchange.Services.Wcf.EWSService.GetServiceAsyncResult[TSoapResponseBody](IAsyncResult result)
    bei Microsoft.Exchange.Services.Wcf.EWSService.CreateSoapResponse[TSoapResponse TSoapResponseBody](IAsyncResult result  Func`2 createSoapResponseCallback)
    bei AsyncInvokeEndEndChannelSubscribe(Object   Object[]   IAsyncResult )
    bei System.ServiceModel.Dispatcher.AsyncMethodInvoker.InvokeEnd(Object instance  Object[]& outputs  IAsyncResult result)
    bei System.ServiceModel.Dispatcher.DispatchOperationRuntime.InvokeEnd(MessageRpc& rpc)
    bei System.ServiceModel.Dispatcher.ImmutableDispatchRuntime.ProcessMessage7(MessageRpc& rpc)
    bei System.ServiceModel.Dispatcher.MessageRpc.Process(Boolean isOperationContextSet);
```

I do not know, if the error messages and the symptom (not updating inboxes) are related, but the “ChannelSubscribe” function sound like I'm on the right path. 

If anyone has an idea, please reach out to me: info@uisa.ch

Philipp